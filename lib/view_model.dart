import 'dart:async';

import 'package:mvvm/repository/repository_memory.dart';
import 'package:mvvm/repository/repository_prefs.dart';
import 'package:mvvm/task_model.dart';

class TaskViewModel {
  final _streamController = StreamController<int>.broadcast();

  Stream<int> get stream => _streamController.stream;

  void dispose() {
    _streamController.close();
  }

  final _repository = RepositoryMemory();

  List<TaskModel> get tasks => _repository.list();
  TaskModel? taskSelected;

  void saveTask(String description) {
    if (taskSelected == null) {
      final id = DateTime.now().microsecondsSinceEpoch.toString();
      _repository.create(TaskModel(description: description, id: id));
    } else {
      _repository.update(taskSelected!);
    }

    _streamController.sink.add(0);
  }

  void removeTask(TaskModel task) {
    if (task == taskSelected) {
      taskSelected = null;
    }
    _repository.remove(task);

    _streamController.sink.add(0);
  }

  void setTaskSelected(TaskModel task) {
    taskSelected = task;
    _streamController.sink.add(0);
  }
}
